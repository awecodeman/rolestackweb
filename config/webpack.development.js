var webpackMerge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var DefinePlugin = require('webpack/lib/DefinePlugin');

var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

module.exports = webpackMerge.smart(commonConfig, {

    devtool: 'cheap-module-eval-source-map',

    output: {
        path: helpers.root('development'),
        filename: 'js/[name].bundle.js',
        sourceMapFilename: 'js/[name].map',
        chunkFilename: 'js/[id].chunk.js'
    },

    plugins: [
        new ExtractTextPlugin('css/app.css'),
        new DefinePlugin({
            'process.env': {
                'enableProdMode': JSON.stringify(false),
                'APIEndpoint': JSON.stringify('http://homestead.app/api/'),
                'ANALYTICS_KEY': JSON.stringify('UA-78195477-X')
            }
        })
    ],

    devServer: {
        port: 3000,
        host: 'localhost',
        historyApiFallback: true,
        inline: true,
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000
        },
        outputPath: helpers.root('development')
    }
});