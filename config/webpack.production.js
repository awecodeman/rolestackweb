var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var DefinePlugin = require('webpack/lib/DefinePlugin');

var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

const ENV = process.env.NODE_ENV = process.env.ENV = 'production';

module.exports = webpackMerge(commonConfig, {
    devtool: 'source-map',

    output: {
        path: helpers.root('production'),
        filename: 'js/[name].[hash].js',
        chunkFilename: '[id].[hash].chunk.js'
    },

    plugins: [
        new DefinePlugin({
            'process.env': {
                'enableProdMode': JSON.stringify(true),
                'APIEndpoint': JSON.stringify('https://rolestack.com/api/'),
                'ANALYTICS_KEY': JSON.stringify('UA-78195477-3')
            }
        }),
        new webpack.NoErrorsPlugin(),
        // new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin({ // https://github.com/angular/angular/issues/10618
            beautify: false,
            compress: {screw_ie8: true},
            mangle: {
                screw_ie8: true,
                keep_fnames: true
            }
        }),
        new webpack.LoaderOptionsPlugin({
            // test: /\.xxx$/, // may apply this only for some modules
            options: {
                htmlLoader: {
                    minimize: false // workaround for ng2
                },
            }
        }),
        new ExtractTextPlugin('[name].[hash].css'),
    ]
});