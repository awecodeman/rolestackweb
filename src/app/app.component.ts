import {Component, OnInit, Inject}      from '@angular/core';
import {Title} from "@angular/platform-browser";
import {Router, NavigationEnd} from "@angular/router";

require('../assets/scss/app.scss');

@Component({
    selector: 'rs-app',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {

    constructor(private titleService: Title, private router:Router, @Inject('ANALYTICS_KEY') private ANALYTICS_KEY) {
    }

    ngOnInit(): void {
        this.titleService.setTitle("A better way to run your RPG adventures");

        ga('create', this.ANALYTICS_KEY, 'auto');
        ga('send', 'pageview');

        this.router.events.subscribe(val => {
            if (val instanceof NavigationEnd) {
                ga('set', 'page', val.urlAfterRedirects);
                ga('send', 'pageview');
            }
        });
    }
}