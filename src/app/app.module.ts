import {NgModule, enableProdMode}      from '@angular/core';
import {BrowserModule, Title} from '@angular/platform-browser';
import {routing} from "./app.routing";

import {AppComponent}   from './app.component';
import {SharedModule} from "./shared/index";
import {AuthModule, AdventureModule} from "./modules/index";
import {RouterModule} from "@angular/router";
import {HttpModule, RequestOptions, XHRBackend} from "@angular/http";
import {AuthHttp, AuthGuard, GuestGuard, AuthService} from "./core/index";
import {HomeComponent} from "./home.component";
import {TitleService} from "./shared/services/title.service";
import {WidgetService} from "./shared/widgets/widget.service";

if (process.env.enableProdMode) {
    enableProdMode();
}

@NgModule({
    imports: [BrowserModule, RouterModule, HttpModule, SharedModule, AuthModule, AdventureModule, routing],
    declarations: [AppComponent, HomeComponent],
    providers: [
        {provide: 'APIEndpoint', useValue: process.env.APIEndpoint},
        {provide: 'ANALYTICS_KEY', useValue: process.env.ANALYTICS_KEY},
        {
            provide: AuthHttp,
            deps: [XHRBackend, RequestOptions, AuthService],
            useFactory: (backend: XHRBackend, defaultOptions: RequestOptions, authService: AuthService) => new AuthHttp(backend, defaultOptions, authService)
        },
        {provide: Title, useFactory: () => new TitleService()},
        GuestGuard, AuthGuard, AuthService, WidgetService],
    bootstrap: [AppComponent]
})
export class AppModule {
}