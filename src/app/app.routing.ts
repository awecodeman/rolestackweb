import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent, RegisterComponent} from "./modules/auth/index";
import {AuthGuard, GuestGuard} from "./core/index";
import {ForgotPasswordComponent} from "./modules/auth/forgot-password/forgot-password.component";
import {ResetPasswordComponent} from "./modules/auth/reset-password/reset-password.component";
import {MarkdownComponent} from "./shared/pages/markdown/markdown.component";

const appRoutes: Routes = [
    {path: 'home', redirectTo: '/home/adventures', canActivate: [AuthGuard]},
    {path: 'login', component: LoginComponent, canActivate: [GuestGuard]},
    {path: 'register', component: RegisterComponent, canActivate: [GuestGuard]},
    {path: 'markdown', component: MarkdownComponent},
    {path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [GuestGuard]},
    {path: 'reset-password/:token', component: ResetPasswordComponent, canActivate: [GuestGuard]},
    {path: '**', redirectTo: '/login'}
];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
