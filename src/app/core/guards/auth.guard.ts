import {Injectable}     from '@angular/core';
import {CanActivate, Router}    from '@angular/router';
import {AuthService} from "../index";

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private authService: AuthService, private router: Router) {
    }

    canActivate() {

        if (!this.isAuthenticated()) {
            this.router.navigate(['/auth/login']);
            return false;
        }
        return true;
    }

    private isAuthenticated() {
        return this.authService.isAuthenticated();
    }
}
