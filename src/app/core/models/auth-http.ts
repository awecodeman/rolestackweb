import {Http, ConnectionBackend, RequestOptions} from "@angular/http";
import {Injectable, Inject} from "@angular/core";
import {AuthService} from "../index";

@Injectable()
export class AuthHttp extends Http {

    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions, authService: AuthService) {
        let token = authService.getToken();
        let tokenString = token.token_type + ' ' + token.access_token;

        defaultOptions.headers.append('Authorization', tokenString);
        defaultOptions.headers.append('Content-Type', 'application/json');
        defaultOptions.headers.append('Accept', 'application/json');
        super(backend, defaultOptions);
    }
}