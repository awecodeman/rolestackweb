export class AuthToken
{
    token_type: string;
    access_token: string;
    expires_in: number;
    refresh_token: string;
    created_at: any;
}