import {Injectable, Inject} from "@angular/core";
import {Headers, Http, Response} from "@angular/http";
import {AuthToken} from "../models/auth-token";

@Injectable()
export class AuthService {
    private headers: Headers = new Headers();

    constructor(private http: Http, @Inject('APIEndpoint') private apiUrl: string) {
        this.headers.append('Content-Type', 'application/json');
        this.headers.append('Accept', 'application/json');
    }

    register(user: UserRegisterCredentials) {
        return this.http.post(this.apiUrl + 'v1/users', user, {headers: this.headers}).map(res => res.json());
    }

    login(user: UserLoginCredentials) {
        return this.http.post(this.apiUrl + 'v1/users/' + user.username + '/login', user, {headers: this.headers}).map(res => this.saveToken(res));
    }

    sendPasswordLink(user: UserLoginCredentials) {
        return this.http.post(this.apiUrl + 'v1/users/' + user.username + '/send-password-link', user, {headers: this.headers}).map(res => res.json());
    }

    resetPasswordLink(user: ResetPasswordCredentials) {
        return this.http.post(this.apiUrl + 'v1/users/' + user.email + '/reset-password', user, {headers: this.headers}).map(res => res.json());
    }

    private saveToken(res: Response) {
        let json = res.json();

        let token: AuthToken = json.data as AuthToken;
        token.created_at = new Date().getTime();

        localStorage.setItem('token', JSON.stringify(token));
        return json;
    }

    isAuthenticated() {
        let token: AuthToken = this.getToken();

        return token != null;
    }

    logout() {
        localStorage.removeItem('token');
    }

    getToken(): AuthToken {
        return JSON.parse(localStorage.getItem('token')) as AuthToken;
    }
}

export class UserRegisterCredentials {
    email: string;
    password: string;
    password_confirmation: string;
}

export class ResetPasswordCredentials {
    email: string;
    password: string;
    password_confirmation: string;
    token: string;
}

export class UserLoginCredentials {
    client_id = 1;
    client_secret = 'mq74Jk8PNItzSaHiVUktlSkMHj1cfbnaTwl0hWjl';
    grant_type = 'password';
    username: string;
    password: string;
}