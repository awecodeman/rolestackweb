import {Component, OnInit, Inject}      from '@angular/core';
import {AuthHttp} from "./core/models/auth-http";
import {Title} from "@angular/platform-browser";

@Component({
    templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

    constructor(private titleService: Title) {
    }

    ngOnInit(): void {
        this.titleService.setTitle("A better way to run your RPG adventures");
    }
}