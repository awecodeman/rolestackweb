import {NgModule}      from '@angular/core';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {adventureRouting} from "./adventure.routing";
import {AdventureListComponent} from "./list/adventure-list.component";
import {AdventureService} from "./shared/adventure.service";
import {SharedModule} from "../../shared/shared.module";
import {AdventureCardComponent} from "./card/adventure-card.component";
import {AdventureEditComponent} from "./edit/adventure-edit.component";
import {RouterModule} from "@angular/router";
import {ContentEditDirective} from "./directives/content-edit.directive";
import {EpisodeEditComponent} from "./episodes/edit/episode-edit.component";
import {AdventurePlayComponent} from "./play/adventure-play.component";
import {EpisodePlayComponent} from "./episodes/play/episode-play.component";
import {PreviewDirective} from "./shared/preview.directive";


@NgModule({
    imports: [CommonModule, FormsModule, RouterModule, SharedModule, adventureRouting],
    declarations: [AdventureListComponent, AdventureCardComponent, AdventureEditComponent,
        ContentEditDirective, EpisodeEditComponent, AdventurePlayComponent, EpisodePlayComponent, PreviewDirective],
    exports: [ContentEditDirective],
    providers: [AdventureService]
})
export class AdventureModule {
}