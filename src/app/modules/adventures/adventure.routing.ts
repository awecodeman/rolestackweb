import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from "../../home.component";
import {AuthGuard, GuestGuard} from "../../core/index";
import {AdventureListComponent} from "./list/adventure-list.component";
import {AdventureEditComponent} from "./edit/adventure-edit.component";
import {EpisodeEditComponent} from "./episodes/edit/episode-edit.component";
import {AdventurePlayComponent} from "./play/adventure-play.component";
import {EpisodePlayComponent} from "./episodes/play/episode-play.component";

const appRoutes: Routes = [
    {
        path: 'home/adventures', component: HomeComponent, canActivate: [AuthGuard], children: [
        {path: ':id/edit', component: AdventureEditComponent, canActivate: [AuthGuard]},
        {path: ':id', component: AdventurePlayComponent, canActivate: [AuthGuard]},
        {path: ':id/chapters/:chapterId/edit', component: EpisodeEditComponent, canActivate: [AuthGuard]},
        {path: ':id/chapters/:chapterId', component: EpisodePlayComponent, canActivate: [AuthGuard]},
        {path: '', component: AdventureListComponent, canActivate: [AuthGuard]},
        {path: '**', redirectTo: '/home/adventures'}]}
    ];

export const appRoutingProviders: any[] = [];

export const adventureRouting: ModuleWithProviders = RouterModule.forChild(appRoutes);
