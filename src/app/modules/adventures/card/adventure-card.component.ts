import {Component, OnInit, Input, EventEmitter}   from '@angular/core';
import {AdventureService} from "../shared/adventure.service";
import {Adventure} from "../shared/adventure";
import moment = require("moment");
import {Output} from "@angular/core/src/metadata/directives";
import {Router} from "@angular/router";

@Component({
    selector: 'rs-adventure-card',
    templateUrl: './adventure-card.component.html'
})
export class AdventureCardComponent implements OnInit {

    @Input()
    private adventure: Adventure = new Adventure;

    @Output()
    private onDeleted = new EventEmitter<Adventure>();

    private inProgress = false;
    private secondsAsDeleted = 5;

    constructor(private adventureService: AdventureService, private router: Router) {

    }

    ngOnInit(): void {
        this.refreshTimers();
    }

    formatDate(date) {
        return moment(date).fromNow();
    }

    deleteAdventure(adventure: Adventure) {
        this.inProgress = true;
        this.adventureService.deleteAdventure(adventure)
            .subscribe(success => {
                    this.adventure.deleted_at = success.data.deleted_at;
                    this.startDeletedCountdown();
                },
                errors => console.log(errors),
                () => this.inProgress = false);
    }

    restoreAdventure(adventure: Adventure) {
        this.inProgress = true;
        this.adventure.deleted_at = null;
        this.adventureService.restoreAdventure(adventure)
            .subscribe(success => {
                    this.adventure.deleted_at = success.data.deleted_at;
                    this.resetDeletedCountdown();
                },
                errors => console.log(errors),
                () => this.inProgress = false);
    }

    private refreshTimers() {
        setTimeout(() => this.refreshTimers(), 5000)
    }

    private startDeletedCountdown() {
        if (this.adventure.deleted_at) {
            this.runCountdown();
        }
        else {
            this.resetDeletedCountdown()
        }
    }

    private runCountdown() {
        setTimeout(() => {
            if (this.adventure.deleted_at) {
                if (this.secondsAsDeleted - 1 <= 0) {
                    this.onDeleted.emit(this.adventure);
                }
                else {
                    this.secondsAsDeleted--;
                    this.runCountdown();
                }
            }
            else {
                this.resetDeletedCountdown()
            }
        }, 1000);
    }

    private resetDeletedCountdown() {
        this.secondsAsDeleted = 5;
    }
}