import {Component, OnInit, ViewChild, EventEmitter}   from '@angular/core';
import moment = require("moment");
import {NgModel} from "@angular/forms";
import {Content} from "../shared/content";
import {Input, Output} from "@angular/core/src/metadata/directives";

@Component({
    selector: 'rs-content-edit',
    templateUrl: './content-edit.directive.html',
    styles: []
})
export class ContentEditDirective implements OnInit {

    @Input() content: Content = new Content;
    @Output() onUpdate = new EventEmitter<Content>();

    @ViewChild('body') body: NgModel;

    constructor() {
    }

    ngOnInit(): void {
        this.body.valueChanges
            .skip(1)
            .debounceTime(1000)
            .subscribe(newValue => this.onUpdate.emit(this.content));

        this.refreshTimers();
    }

    private refreshTimers() {
        setTimeout(() => this.refreshTimers(), 5000)
    }
}