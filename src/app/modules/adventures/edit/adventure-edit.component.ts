import {Component, OnInit, ViewChild, AfterViewInit}   from '@angular/core';
import {AdventureService} from "../shared/adventure.service";
import {Adventure} from "../shared/adventure";
import moment = require("moment");
import {Route, ActivatedRoute} from "@angular/router";
import {NgControl, NgModel, NgForm} from "@angular/forms";
import {Episode} from "../shared/episode";
import {Title} from "@angular/platform-browser";

@Component({
    templateUrl: './adventure-edit.component.html'
})
export class AdventureEditComponent implements OnInit {

    @ViewChild('title') title: NgModel;

    private adventure: Adventure = new Adventure;

    private isPageLoading = true;
    private isAddingEpisode = false;
    private isSaving = false;
    private errors: any = null;

    constructor(private adventureService: AdventureService, private route: ActivatedRoute, private titleService: Title) {
    }

    ngOnInit(): void {
        this.titleService.setTitle("Edit adventure");

        this.route.params.subscribe(params => this.loadAdventure(params['id']));

        this.title.valueChanges
            .skip(2)
            .debounceTime(1000)
            .subscribe(newValue => this.updateAdventure(this.adventure));

        this.refreshTimers();
    }

    addEpisode() {
        this.isAddingEpisode = true;

        this.adventureService.addEpisode(this.adventure)
            .subscribe(success => this.adventure.episodes.push(success.data),
                errors => console.log(errors),
                () => this.isAddingEpisode = false);
    }

    deleteEpisode(episode: Episode) {
        episode.deleted_at = '1';

        this.adventureService.deleteEpisode(episode)
            .subscribe(success => {
                    episode.deleted_at = success.data.deleted_at;

                    setTimeout(() => {
                        if (episode.deleted_at) {
                            this.adventure.episodes.splice(this.adventure.episodes.indexOf(episode), 1);
                        }
                    }, 5000);
                },
                errors => console.log(errors),
                () => null);
    }

    restoreEpisode(episode: Episode) {
        episode.deleted_at = null;

        this.adventureService.restoreEpisode(episode)
            .subscribe(success => {
                    episode.deleted_at = success.data.deleted_at;
                },
                errors => console.log(errors),
                () => null);
    }

    private loadAdventure(id: string) {
        this.adventureService.getAdventure(id)
            .finally(() => this.isPageLoading = false)
            .subscribe(success => this.adventure = success.data,
                errors => this.errors = errors);
    }

    private updateAdventure(adventure: Adventure) {
        this.isSaving = true;
        this.adventureService.updateAdventure(adventure)
            .subscribe(success => {
                    this.adventure.content.html = success.data.content.html;
                    this.adventure.updated_at = success.data.updated_at
                },
                errors => console.log(errors),
                () => this.isSaving = false);
    }

    formatDate(date) {
        return moment(date).fromNow();
    }

    private refreshTimers() {
        setTimeout(() => this.refreshTimers(), 5000)
    }
}