import {Component, OnInit, ViewChild, AfterViewInit}   from '@angular/core';
import moment = require("moment");
import {Route, ActivatedRoute} from "@angular/router";
import {NgControl, NgModel, NgForm} from "@angular/forms";
import {Adventure} from "../../shared/adventure";
import {AdventureService} from "../../shared/adventure.service";
import {Episode} from "../../shared/episode";
import {Observable, Subscription} from "rxjs";
import {Title} from "@angular/platform-browser";

@Component({
    templateUrl: './episode-edit.component.html'
})
export class EpisodeEditComponent implements OnInit {

    @ViewChild('title') title: NgModel;

    private episode: Episode = new Episode;
    private adventure: Adventure = new Adventure;

    private isPageLoading = true;
    private isSaving = false;
    private errors: any = null;
    private subscription: Subscription;

    constructor(private adventureService: AdventureService, private route: ActivatedRoute, private titleService: Title) {
    }

    ngOnInit(): void {
        this.titleService.setTitle("Edit chapter");

        this.route.params.subscribe(params => {

            if (this.subscription && !this.subscription.closed) {
                this.subscription.unsubscribe();
            }

            this.isPageLoading = true;

            Observable.zip(this.adventureService.getEpisode(params['chapterId']), this.adventureService.getAdventure(params['id']),
                (episode, adventure) => {
                    return {adventure: adventure.data, episode: episode.data};
                })
                .finally(() => this.isPageLoading = false)
                .subscribe(success => {
                        this.adventure = success.adventure;
                        this.episode = success.episode;

                        this.subscription = this.title.valueChanges
                            .skip(2)
                            .debounceTime(1000)
                            .subscribe(newValue => this.updateEpisode(this.episode));
                    },
                    errors => this.errors = errors
                )
            ;
        });
    }

    private updateEpisode(episode: Episode) {
        this.isSaving = true;
        this.adventureService.updateEpisode(episode)
            .subscribe(success => {
                    this.episode.content.html = success.data.content.html;
                    this.episode.updated_at = success.data.updated_at;

                    this.updateTitleInList(episode);
                },
                errors => console.log(errors),
                () => this.isSaving = false);
    }

    private updateTitleInList(episode: Episode) {
        for (var i = 0; i < this.adventure.episodes.length; i++) {
            var e = this.adventure.episodes[i];
            if (episode.id == e.id) {
                e.title = episode.title;
            }
        }
    }

    formatDate(date) {
        return moment(date).fromNow();
    }
}