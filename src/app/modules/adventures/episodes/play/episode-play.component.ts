import {Component, OnInit, ViewChild, AfterViewInit}   from '@angular/core';
import moment = require("moment");
import {Route, ActivatedRoute} from "@angular/router";
import {NgControl, NgModel, NgForm} from "@angular/forms";
import {AdventureService} from "../../shared/adventure.service";
import {Adventure} from "../../shared/adventure";
import {Episode} from "../../shared/episode";
import {Observable} from "rxjs";
import {Title} from "@angular/platform-browser";

@Component({
    templateUrl: './episode-play.component.html'
})
export class EpisodePlayComponent implements OnInit {

    private adventure: Adventure = new Adventure;
    private episode: Episode = new Episode;

    private nextEpisode: Episode = null;
    private previousEpisode: Episode = null;

    private isPageLoading = true;
    private errors: any = null;

    constructor(private adventureService: AdventureService, private route: ActivatedRoute, private titleService: Title) {
    }

    ngOnInit(): void {
        this.titleService.setTitle("Chapter");

        this.route.params.subscribe(params => {
            this.isPageLoading = true;

            Observable.zip(this.adventureService.getEpisode(params['chapterId']), this.adventureService.getAdventure(params['id']),
                (episode, adventure) => {
                    return {adventure: adventure.data, episode: episode.data};
                })
                .finally(() => this.isPageLoading = false)
                .subscribe(success => {
                        this.adventure = success.adventure;
                        this.episode = success.episode;
                        this.nextEpisode = this.getNextEpisode();
                        this.previousEpisode = this.getPreviousEpisode();
                        this.titleService.setTitle(this.episode.title ? this.episode.title : 'Untitled chapter');

                    },
                    errors => this.errors = errors);
        });
    }

    getNextEpisode() {
        for (var i = 0; i < this.adventure.episodes.length; i++) {
            var episode = this.adventure.episodes[i];

            if (episode.id == this.episode.id && i < this.adventure.episodes.length) {
                return this.adventure.episodes[i + 1];
            }
        }
        return null;
    }

    getPreviousEpisode() {
        for (var i = 0; i < this.adventure.episodes.length; i++) {
            var episode = this.adventure.episodes[i];

            if (episode.id == this.episode.id && i > 0) {
                return this.adventure.episodes[i - 1];
            }
        }
        return null;
    }
}