import {Component, OnInit}   from '@angular/core';
import {AdventureService} from "../shared/adventure.service";
import {Adventure} from "../shared/adventure";
import {Router} from "@angular/router";
import {Title} from "@angular/platform-browser";

@Component({
    selector: 'rs-adventure-list',
    templateUrl: './adventure-list.component.html'
})
export class AdventureListComponent implements OnInit {

    private isPageLoading = true;
    private isCreating = false;
    private adventures: Adventure[] = [];

    constructor(private adventureService: AdventureService, private router: Router, private titleService:Title) {
    }

    ngOnInit(): void {
        this.titleService.setTitle("Adventures");
        this.adventureService.getList()
            .subscribe(success => this.adventures = success.data, error => console.log(error), () => this.isPageLoading = false);
    }

    createAdventure(): void {
        this.isCreating = true;
        this.adventureService.createAdventure()
            .subscribe(success => {
                this.adventures.push(success.data);
                this.router.navigate(['/home/adventures', success.data.id, 'edit'])
            }, error => console.log(error), () => this.isCreating = false);
    }

    onAdventureDeleted(adventure: Adventure): void {
        this.adventures.splice(this.adventures.indexOf(adventure), 1);
    }
}