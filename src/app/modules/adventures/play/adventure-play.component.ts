import {Component, OnInit, ViewChild, AfterViewInit}   from '@angular/core';
import {AdventureService} from "../shared/adventure.service";
import {Adventure} from "../shared/adventure";
import moment = require("moment");
import {Route, ActivatedRoute} from "@angular/router";
import {NgControl, NgModel, NgForm} from "@angular/forms";
import {Episode} from "../shared/episode";
import {Title} from "@angular/platform-browser";
import {WidgetService} from "../../../shared/widgets/widget.service";

@Component({
    templateUrl: './adventure-play.component.html'
})
export class AdventurePlayComponent implements OnInit {

    @ViewChild('title') title: NgModel;

    private adventure: Adventure = new Adventure;
    private nextEpisode: Episode = null;

    private isPageLoading = true;
    private errors: any = null;

    constructor(private adventureService: AdventureService, private route: ActivatedRoute, private titleService: Title) {
    }

    ngOnInit(): void {
        this.titleService.setTitle("Adventure");
        this.route.params.subscribe(params => this.loadAdventure(params['id']));
    }
    private loadAdventure(id: string) {
        this.adventureService.getAdventure(id)
            .finally(() => this.isPageLoading = false)
            .subscribe(success => {
                    this.adventure = success.data;
                    this.titleService.setTitle(this.adventure.title ? this.adventure.title : 'Untitled adventure');

                    if (this.adventure.episodes.length > 0) {
                        this.nextEpisode = this.adventure.episodes[0];
                    }
                },
                errors => this.errors = errors);
    }

}