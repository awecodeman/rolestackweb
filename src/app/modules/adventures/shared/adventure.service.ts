import {Injectable, Inject} from "@angular/core";
import {AuthHttp} from "../../../core/models/auth-http";
import {Adventure} from "./adventure";
import {Episode} from "./episode";

@Injectable()
export class AdventureService
{
    constructor(private http: AuthHttp, @Inject('APIEndpoint') private apiUrl: string) {
    }

    getList() {
        return this.http.get(this.apiUrl + 'v1/adventures').map(res => res.json());
    }

    createAdventure() {
        return this.http.post(this.apiUrl + 'v1/adventures', {}).map(res => res.json());
    }

    deleteAdventure(adventure: Adventure) {
        return this.http.delete(this.apiUrl + 'v1/adventures/' + adventure.id, {}).map(res => res.json());
    }

    restoreAdventure(adventure: Adventure) {
        return this.http.put(this.apiUrl + 'v1/adventures/' + adventure.id + '/restore', {}).map(res => res.json());
    }

    getAdventure(id: string) {
        return this.http.get(this.apiUrl + 'v1/adventures/' + id, {}).map(res => res.json());
    }

    updateAdventure(adventure: Adventure) {
        return this.http.put(this.apiUrl + 'v1/adventures/' + adventure.id, adventure).map(res => res.json());
    }

    addEpisode(adventure: Adventure) {
        return this.http.post(this.apiUrl + 'v1/adventures/' + adventure.id + '/episodes', {}).map(res => res.json());
    }

    deleteEpisode(episode: Episode) {
        return this.http.delete(this.apiUrl + 'v1/episodes/' + episode.id, {}).map(res => res.json());
    }

    restoreEpisode(episode: Episode) {
        return this.http.put(this.apiUrl + 'v1/episodes/' + episode.id + '/restore', {}).map(res => res.json());
    }

    getEpisode(id: string) {
        return this.http.get(this.apiUrl + 'v1/episodes/' + id, {}).map(res => res.json());
    }

    updateEpisode(episode: Episode) {
        return this.http.put(this.apiUrl + 'v1/episodes/' + episode.id, episode).map(res => res.json());
    }
}