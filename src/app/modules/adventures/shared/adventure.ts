import {Episode} from "./episode";
import {Content} from "./content";

export class Adventure
{
    id: string;
    title: string;
    content: Content = new Content;
    episodes: Episode[] = [];
    created_at: string;
    updated_at: string;
    deleted_at: string;
}