export class Content
{
    id: string;
    body: string;
    html: string;
    created_at: string;
    updated_at: string;
}