import {Content} from "./content";

export class Episode
{
    id: string;
    title: string;
    content: Content = new Content;
    created_at: string;
    updated_at: string;
    deleted_at: string;
}