import {
    Component, OnInit, ViewChild, EventEmitter, ElementRef, Renderer, AfterViewInit,
    OnChanges, SimpleChanges
}   from '@angular/core';
import moment = require("moment");
import {Directive, HostListener, Input, HostBinding} from "@angular/core/src/metadata/directives";
import {Observable} from "rxjs";

@Directive({
    selector: '[rs-preview]',
})
export class PreviewDirective implements OnInit, OnChanges {

    @Input() html: any;

    ngOnChanges(changes: SimpleChanges): void {
        this.el.nativeElement.innerHTML = changes['html'].currentValue;

        setTimeout(() => this.resizePreview(), 0);
    }

    @HostListener('window:resize', ['$event.target'])
    onResize()
    {
        this.resizePreview();
    }

    constructor(private el: ElementRef, private renderer: Renderer) {
    }

    ngOnInit(): void {
    }

    private resizePreview() {
        this.renderer.setElementStyle(this.el.nativeElement, 'width', '100%');
        let scale = this.el.nativeElement.offsetWidth / 730;
        this.renderer.setElementStyle(this.el.nativeElement, 'width', '730px');
        this.renderer.setElementStyle(this.el.nativeElement.parentElement, 'height', (this.el.nativeElement.offsetHeight * scale + 60) + 'px');
        this.renderer.setElementStyle(this.el.nativeElement, 'transform', 'scale(' + scale + ')');
        this.renderer.setElementStyle(this.el.nativeElement, 'transform-origin', '0 0');
    }
}