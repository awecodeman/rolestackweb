import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {SharedModule} from "../../shared/index";

import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {ForgotPasswordComponent} from "./forgot-password/forgot-password.component";
import {ResetPasswordComponent} from "./reset-password/reset-password.component";

@NgModule({
    imports: [BrowserModule, SharedModule],
    declarations: [LoginComponent, RegisterComponent, ForgotPasswordComponent, ResetPasswordComponent]
})
export class AuthModule {
}