import {Component, OnInit}   from '@angular/core';
import {AuthService} from "../../../core/services/auth.service";
import {UserLoginCredentials} from "../../../core/index";
import {NgForm} from "@angular/forms";
import {ViewChild} from "@angular/core/src/metadata/di";
import {Router, ActivatedRoute} from "@angular/router";
import {Title} from "@angular/platform-browser";

@Component({
    templateUrl: './forgot-password.component.html'
})
export class ForgotPasswordComponent implements OnInit {

    @ViewChild('forgotPasswordForm') forgotPasswordForm: NgForm;

    private user = new UserLoginCredentials;

    private isSuccess = false;
    private inProgress = false;
    private errors = {};

    constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute, private titleService:Title) {
        this.route.params.map(params => params['email'])
            .subscribe(email => {
                this.user.username = email;
            });
    }

    ngOnInit(): void {
        this.titleService.setTitle("Forgot password");

        //  clear any errors if the form changes
        this.forgotPasswordForm.valueChanges
            .subscribe(newValue => this.errors = {});
    }

    onSubmit(): void {
        if (!this.forgotPasswordForm.valid) {
            this.showFormValidation();
        }
        else {
            this.sendPasswordLink(this.user);
        }
    }

    private showFormValidation() {
        for (var key in this.forgotPasswordForm.controls) {
            if (this.forgotPasswordForm.controls.hasOwnProperty(key)) {
                this.forgotPasswordForm.controls[key].markAsDirty();
            }
        }
    }

    private sendPasswordLink(user: UserLoginCredentials) {
        this.inProgress = true;
        this.authService.sendPasswordLink(user)
            .finally(() => this.inProgress = false)
            .subscribe(data => this.isSuccess = true, errors => this.showErrors(errors.json()));
    }

    private showErrors(errors: any) {
        this.errors = errors.data.errors;
    }
}