import {Component, OnInit}   from '@angular/core';
import {AuthService} from "../../../core/services/auth.service";
import {UserLoginCredentials} from "../../../core/index";
import {NgForm} from "@angular/forms";
import {ViewChild} from "@angular/core/src/metadata/di";
import {Router, ActivatedRoute} from "@angular/router";
import {Title} from "@angular/platform-browser";

@Component({
    templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

    @ViewChild('loginForm') loginForm: NgForm;

    private user = new UserLoginCredentials;

    private inProgress = false;
    private errors = {};

    constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute, private titleService:Title) {
        this.route.params.map(params => params['email'])
            .subscribe(email => {
                this.user.username = email;
            });
    }

    ngOnInit(): void {
        this.titleService.setTitle("Login");

        //  clear any errors if the form changes
        this.loginForm.valueChanges
            .subscribe(newValue => this.errors = {});
    }

    onSubmit(): void {
        if (!this.loginForm.valid) {
            this.showFormValidation();
        }
        else {
            this.login(this.user);
        }
    }

    private showFormValidation() {
        for (var key in this.loginForm.controls) {
            if (this.loginForm.controls.hasOwnProperty(key)) {
                this.loginForm.controls[key].markAsDirty();
            }
        }
    }

    private login(user: UserLoginCredentials) {
        this.inProgress = true;
        this.authService.login(user)
            .finally(() => this.inProgress = false)
            .subscribe(success => this.router.navigate(['/home']), errors => this.showErrors(errors.json()));
    }

    private showErrors(errors: any) {
        this.errors = errors.data.errors;
    }
}