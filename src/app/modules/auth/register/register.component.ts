import {Component, OnInit}   from '@angular/core';
import {NgForm} from "@angular/forms";
import {UserRegisterCredentials, UserLoginCredentials} from "../../../core/index";
import {ViewChild} from "@angular/core/src/metadata/di";
import {AuthService} from "../../../core/services/auth.service";
import {Router, ActivatedRoute} from "@angular/router";
import {Title} from "@angular/platform-browser";

@Component({
    templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit{

    @ViewChild('registerForm') registerForm: NgForm;

    private user = new UserRegisterCredentials;

    private inProgress = false;
    private errors = {};

    constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute, private titleService:Title) {
        this.route.params.map(params => params['email'])
            .subscribe(email => {
                this.user.email = email;
            });
    }

    ngOnInit(): void {
        this.titleService.setTitle("Register");

        //  clear any errors if the form changes
        this.registerForm.valueChanges
            .subscribe(newValue => this.errors = {});
    }

    onSubmit(): void {
        if (!this.registerForm.valid) {
            this.showFormValidation();
        }
        else {
            this.register(this.user);
        }
    }

    private showFormValidation() {
        for (var key in this.registerForm.controls) {
            if (this.registerForm.controls.hasOwnProperty(key)) {
                this.registerForm.controls[key].markAsDirty();
            }
        }
    }

    private register(user: UserRegisterCredentials) {
        this.inProgress = true;
        this.authService.register(user)
            .finally(() => this.inProgress = false)
            .subscribe(success => this.login(user), errors => this.showErrors(errors.json()));
    }

    private showErrors(errors: any) {
        this.errors = errors.data.errors;
    }

    private login(user: UserRegisterCredentials) {

        let loginUser = new UserLoginCredentials;
        loginUser.username = user.email;
        loginUser.password = user.password;

        this.authService.login(loginUser)
            .finally(() => this.inProgress = false)
            .subscribe(success => this.router.navigate(['/home']), errors => this.showErrors(errors.json()));
    }
}