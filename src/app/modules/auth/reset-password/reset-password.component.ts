import {Component, OnInit}   from '@angular/core';
import {AuthService, UserRegisterCredentials, ResetPasswordCredentials} from "../../../core/services/auth.service";
import {UserLoginCredentials} from "../../../core/index";
import {NgForm} from "@angular/forms";
import {ViewChild} from "@angular/core/src/metadata/di";
import {Router, ActivatedRoute} from "@angular/router";
import {Title} from "@angular/platform-browser";

@Component({
    templateUrl: './reset-password.component.html'
})
export class ResetPasswordComponent implements OnInit {

    @ViewChild('resetPasswordForm') resetPasswordForm: NgForm;

    private user = new ResetPasswordCredentials;

    private token;

    private inProgress = false;
    private errors = {};

    constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute, private titleService:Title) {
        this.route.params
            .subscribe(params => {
                this.token = params['token'];
            });
    }

    ngOnInit(): void {
        this.titleService.setTitle("Reset password");

        //  clear any errors if the form changes
        this.resetPasswordForm.valueChanges
            .subscribe(newValue => this.errors = {});
    }

    onSubmit(): void {
        if (!this.resetPasswordForm.valid) {
            this.showFormValidation();
        }
        else {
            this.resetPasswordLink(this.user);
        }
    }

    private showFormValidation() {
        for (var key in this.resetPasswordForm.controls) {
            if (this.resetPasswordForm.controls.hasOwnProperty(key)) {
                this.resetPasswordForm.controls[key].markAsDirty();
            }
        }
    }

    private resetPasswordLink(user: ResetPasswordCredentials) {
        user.token = this.token;
        this.inProgress = true;
        this.authService.resetPasswordLink(user)
            .finally(() => this.inProgress = false)
            .subscribe(data => this.login(user), errors => this.showErrors(errors.json()));
    }

    private showErrors(errors: any) {
        this.errors = errors.data.errors;
    }

    private login(user: ResetPasswordCredentials) {

        let loginUser = new UserLoginCredentials();
        loginUser.password = user.password;
        loginUser.username = user.email;

        this.authService.login(loginUser)
            .finally(() => this.inProgress = false)
            .subscribe(success => this.router.navigate(['/home']), errors => this.showErrors(errors.json()));
    }
}