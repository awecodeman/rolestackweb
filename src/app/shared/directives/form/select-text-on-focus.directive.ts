import {Directive, ElementRef, Renderer,} from '@angular/core';
@Directive({
    selector: '[select-text-on-focus]',
    host: {
        '(focus)': 'selectText()',
    }
})
export class SelectTextOnFocusDirective {

    constructor(private el: ElementRef, private renderer: Renderer) {
    }

    selectText() {
        this.el.nativeElement.setSelectionRange(0, this.el.nativeElement.value.length);
    }
}