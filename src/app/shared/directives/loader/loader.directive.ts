import {Component, OnInit, Input} from '@angular/core';

@Component({
    selector: 'rs-loader',
    template: require('./loader.directive.html'),
    styles: [`.loading {
              display: inline-block;
              border-width: 18px;
              border-radius: 50%;
              margin: 0 1rem -5px 1rem;
              -webkit-animation: spin 1s ease-in-out infinite;
                 -moz-animation: spin 1s ease-in-out infinite;
                   -o-animation: spin 1s ease-in-out infinite;
                      animation: spin 1s ease-in-out infinite;
              }
            
            .spin {
              border-style: solid;
              border-color: #f6a821 transparent;
              }

            @-webkit-keyframes spin {
              100% { -webkit-transform: rotate(359deg); }
              }
            
            @-moz-keyframes spin {
              100% { -moz-transform: rotate(359deg); }
              }
            
            @-o-keyframes spin {
              100% { -moz-transform: rotate(359deg); }
              }
            
            @keyframes spin {
              100% {  transform: rotate(359deg); }
              }`]
})
export class LoaderDirective implements OnInit {

    @Input()
    size: number = 18;

    ngOnInit() {
    }
}