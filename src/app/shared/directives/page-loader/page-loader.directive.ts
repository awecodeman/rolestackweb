import {Component, OnInit, Input} from '@angular/core';

@Component({
    selector: 'rs-page-loader',
    template: require('./page-loader.directive.html')
})
export class PageLoaderDirective implements OnInit {

    ngOnInit() {
    }
}