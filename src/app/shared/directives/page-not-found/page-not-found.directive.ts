import {Component, OnInit, Input} from '@angular/core';

@Component({
    selector: 'rs-page-not-found',
    template: require('./page-not-found.directive.html')
})
export class PageNotFoundDirective implements OnInit {

    @Input()
    text: string = 'Page not found';

    ngOnInit() {
    }
}