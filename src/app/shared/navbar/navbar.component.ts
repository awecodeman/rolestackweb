import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../core/services/auth.service";
import {Router} from "@angular/router";

@Component({
    selector: 'rs-navbar',
    templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

    private isLoggedIn = false;

    constructor(private authService: AuthService, private router: Router) {
    }

    ngOnInit() {
        this.router.events.subscribe(val => this.checkUser());
    }

    logout() {
        this.authService.logout();
    }

    private checkUser() {
        this.isLoggedIn = this.authService.isAuthenticated();
    }
}