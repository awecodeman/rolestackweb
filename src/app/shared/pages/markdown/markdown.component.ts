import {Component, OnInit, Input, EventEmitter}   from '@angular/core';
import {Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {Content} from "../../../modules/adventures/shared/content";

@Component({
    templateUrl: './markdown.component.html'
})
export class MarkdownComponent implements OnInit {

    private headingsContent: Content = new Content;

    constructor(private router: Router, private titleService:Title) {

    }

    ngOnInit(): void {
        this.titleService.setTitle("Markdown");

        this.headingsContent.body = '# Heading 1';
    }
}