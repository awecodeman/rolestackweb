import {Injectable} from "@angular/core";
import {Title} from "@angular/platform-browser";

@Injectable()
export class TitleService extends Title {
    setTitle(newTitle: string) {
        super.setTitle(newTitle + " :: RoleStack");
    }
}