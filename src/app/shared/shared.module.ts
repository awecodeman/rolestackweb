import {NgModule}      from '@angular/core';
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";

import {NavbarComponent, EqualValidatorDirective} from "./index";
import {LoaderDirective} from "./directives/loader/loader.directive";
import {PageLoaderDirective} from "./directives/page-loader/page-loader.directive";
import {PageNotFoundDirective} from "./directives/page-not-found/page-not-found.directive";
import {TooltipModule} from 'ng2-bootstrap/ng2-bootstrap';
import {WidgetOutletComponent} from "./widgets/outlet/widget-outlet.component";
import {DiceWidgetComponent} from "./widgets/dice/dice-widget.component";
import {WidgetDirective} from "./widgets/directives/widget.directive";
import {WidgetDraggableDirective} from "./widgets/directives/widget-draggable.directive";
import {SelectTextOnFocusDirective} from "./directives/form/select-text-on-focus.directive";
import {WidgetOnScrollDirective} from "./widgets/directives/widget-on-scroll.directive";
import {WidgetIconsComponent} from "./widgets/outlet/widget-icons.component";
import {AutoResizeDirective} from "./directives/auto-resize.directive";
import {MarkdownComponent} from "./pages/markdown/markdown.component";


@NgModule({
    imports: [CommonModule, FormsModule, RouterModule,TooltipModule],
    declarations: [WidgetIconsComponent, WidgetOnScrollDirective, SelectTextOnFocusDirective, NavbarComponent, EqualValidatorDirective, LoaderDirective, PageLoaderDirective, PageNotFoundDirective, WidgetOutletComponent,
        DiceWidgetComponent, WidgetDraggableDirective, WidgetDirective, AutoResizeDirective, MarkdownComponent],
    exports: [SelectTextOnFocusDirective,WidgetIconsComponent, CommonModule, FormsModule, RouterModule,
        NavbarComponent, EqualValidatorDirective, LoaderDirective, PageLoaderDirective, PageNotFoundDirective, TooltipModule,
        WidgetOutletComponent, DiceWidgetComponent, WidgetDraggableDirective, WidgetDirective, WidgetOnScrollDirective, AutoResizeDirective, MarkdownComponent]
})
export class SharedModule {
}