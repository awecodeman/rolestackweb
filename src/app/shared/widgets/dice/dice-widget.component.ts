import {Component, OnInit, OnDestroy} from '@angular/core';
import {AuthService} from "../../../core/services/auth.service";
import {Router} from "@angular/router";
import {WidgetService} from "../widget.service";
import {NgModel} from "@angular/forms";
import {ViewChild} from "@angular/core/src/metadata/di";
import {WidgetDirective} from "../directives/widget.directive";

@Component({
    selector: 'rs-dice-widget',
    templateUrl: './dice-widget.component.html'
})
export class DiceWidgetComponent implements OnInit, OnDestroy {

    public numberOfRolls = 1;
    public selectedDiceType = 20;
    public results = [];
    public showResults = false;
    public modifier = 0;

    private name = 'diceWidget';

    @ViewChild('widget') widget:WidgetDirective;

    ngOnInit(): any {
        this.widget.name = this.name;
        this.widget.load();
    }

    ngOnDestroy(): void {
    }

    constructor(private widgetService: WidgetService) {
    }

    closeWidget() {
        this.widgetService.toggleDiceWidget();
    }

    minusRolls(number) {
        if (this.numberOfRolls - number < 1) {
             return this.numberOfRolls = 1;
        }

        return this.numberOfRolls -= number;
    }

    plusRolls(number) {
        return this.numberOfRolls += number;
    }

    minusModifier(number) {
        return this.modifier -= number;
    }

    plusModifier(number) {
        return this.modifier += number;
    }

    isDiceTypeActive(type) {
        return type == this.selectedDiceType;
    }

    selectType(type) {
        this.selectedDiceType = type;
    }

    roll() {
        this.results = [];

        for (var i = 0; i < this.numberOfRolls; i++) {

            this.results.push({
                result: Math.round(Math.random() * (this.selectedDiceType - 1) + 1),
                modifier: this.modifier
            });
        }

        this.showResults = true;
    }

    getSum() {
        var sum = 0;
        for (var i = 0; i < this.results.length; i++) {
            var result = this.results[i];
            sum += (result.result + result.modifier);
        }

        return sum;
    }

    prefixModifier(modifier) {
        if (modifier >= 0) {
            return '+' + modifier;
        }

        return modifier;
    }
}