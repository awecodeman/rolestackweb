import {Directive, ElementRef, Renderer, OnDestroy, OnInit} from '@angular/core';
import {WidgetDirective} from "../directives/widget.directive";
import {WidgetOnScrollDirective} from "./widget-on-scroll.directive";

@Directive({
    selector: '[draggable]',
    host: {
        '(dragstart)': 'onDragStart($event)',
        '(dragend)': 'onDragEnd($event)',
        '(drag)': 'onDrag($event)',
        '(window:resize)': 'onResize($event)'
    }
})
export class WidgetDraggableDirective implements OnDestroy, OnInit {
    private deltaX: number = 0;
    private deltaY: number = 0;

    constructor(private el: ElementRef, private renderer: Renderer, private widget: WidgetDirective) {
    }

    public ngOnInit(): void {
        this.renderer.setElementAttribute(this.el.nativeElement, 'draggable', 'true');

        this.widget.widgetService.onScroll.subscribe(value => this.translatePin(value));

        this.checkVisibleOnScreen();
    }

    onDragStart(event: MouseEvent) {
        this.deltaX = event.x - this.widget.el.nativeElement.offsetLeft;
        this.deltaY = event.y - this.widget.el.nativeElement.offsetTop;
    }

    onDrag(event: MouseEvent) {
        this.doTranslation(event.x, event.y);
    }

    onDragEnd(event: MouseEvent) {
        this.deltaX = 0;
        this.deltaY = 0;
    }

    doTranslation(x: number, y: number) {
        if (!x || !y) return;

        if (x - this.deltaX < 0) {
            x = this.deltaX;
        }
        else if (x - this.deltaX > window.document.body.offsetWidth - this.widget.el.nativeElement.offsetWidth) {
            x = window.document.body.offsetWidth - this.widget.el.nativeElement.offsetWidth + this.deltaX;
        }

        if (y - this.deltaY < 61) {
            y = this.deltaY + 61;
        }
        else if (!this.widget.pinned && y - this.deltaY > window.document.body.offsetHeight - this.widget.el.nativeElement.offsetHeight) {
            y = window.document.body.offsetHeight - this.widget.el.nativeElement.offsetHeight + this.deltaY;
        }

        this.widget.doTranslation(y - this.deltaY, x - this.deltaX);
    }

    onResize(event: Event) {
        this.checkVisibleOnScreen();
    }

    private checkVisibleOnScreen() {
        if (this.widget.el.nativeElement.offsetWidth + this.widget.el.nativeElement.offsetLeft > window.document.body.offsetWidth) {
            this.widget.renderer.setElementStyle(this.widget.el.nativeElement, 'left', (window.document.body.offsetWidth - this.widget.el.nativeElement.offsetWidth) + 'px');
        }

        if (this.widget.widgetService.widgetOnScroll.el.nativeElement.scrollHeight < this.widget.el.nativeElement.offsetHeight + this.widget.el.nativeElement.offsetTop) {
            this.widget.renderer.setElementStyle(this.widget.el.nativeElement, 'top', (this.widget.widgetService.widgetOnScroll.el.nativeElement.scrollHeight - this.widget.el.nativeElement.offsetHeight) + 'px');
        }
    }

    public ngOnDestroy(): void {
        this.renderer.setElementAttribute(this.el.nativeElement, 'draggable', 'false');
    }

    private translatePin(value: any) {
        this.widget.translatePin(value);
    }
}