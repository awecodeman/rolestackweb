import {Directive, ElementRef, Renderer, OnDestroy, OnInit} from '@angular/core';
import {WidgetService} from "../widget.service";

@Directive({
    selector: '[widget-on-scroll]',
    host: {
        '(scroll)': 'onScroll($event)'
    }
})
export class WidgetOnScrollDirective implements OnDestroy, OnInit {

    private lastScroll: any;

    constructor(public el: ElementRef, private renderer: Renderer, private widgetService:WidgetService) {
    }

    public ngOnInit(): void {
        this.lastScroll = this.el.nativeElement.scrollTop;

        this.widgetService.widgetOnScroll = this;
    }

    private onScroll(event: Event) {
        this.widgetService.scrolled(this.el.nativeElement.scrollTop - this.lastScroll);
        this.lastScroll = this.el.nativeElement.scrollTop;
    }

    public ngOnDestroy(): void {
    }
}