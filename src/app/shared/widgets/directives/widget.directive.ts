import {WidgetService} from "../widget.service";
import {Directive} from "@angular/core/src/metadata/directives";
import {ElementRef, Renderer, OnInit, OnDestroy} from "@angular/core";

@Directive({
    selector: '[widget]',
    exportAs: 'WidgetDirective'
})
export class WidgetDirective implements OnInit, OnDestroy {
    public pinned = false;
    public name = '';

    constructor(public el: ElementRef, public renderer: Renderer, public widgetService: WidgetService) {
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }

    togglePinned() {
        this.pinned = !this.pinned;
        this.save();
    }

    load() {
        let settings = JSON.parse(localStorage.getItem(this.name));

        if (settings) {
            this.renderer.setElementStyle(this.el.nativeElement, 'top', (settings.top) + 'px');
            this.renderer.setElementStyle(this.el.nativeElement, 'left', (settings.left) + 'px');
            this.pinned = settings.pinned;
        }
    }

    save() {
        localStorage.setItem(this.name, JSON.stringify({
            top: this.el.nativeElement.offsetTop == 0 ? null: this.el.nativeElement.offsetTop,
            left: this.el.nativeElement.offsetLeft,
            pinned: this.pinned
        }));
    }

    translatePin(value: number) {
        if (this.pinned) {
            this.renderer.setElementStyle(this.el.nativeElement, 'top', this.el.nativeElement.offsetTop - value + 'px');
            this.save();
        }
    }

    doTranslation(y: number, x: number) {
        this.renderer.setElementStyle(this.el.nativeElement, 'top', (y) + 'px');
        this.renderer.setElementStyle(this.el.nativeElement, 'left', (x) + 'px');
        this.save();
    }
}