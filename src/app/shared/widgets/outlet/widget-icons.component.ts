import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../core/services/auth.service";
import {Router} from "@angular/router";
import {WidgetService} from "../widget.service";

@Component({
    selector: 'rs-widget-icons',
    templateUrl: './widget-icons.component.html'
})
export class WidgetIconsComponent implements OnInit {


    constructor(private widgetService:WidgetService) {
    }

    ngOnInit() {
    }

    private toggleDiceWidget()
    {
        this.widgetService.toggleDiceWidget();
    }
}