import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../../core/services/auth.service";
import {Router} from "@angular/router";
import {WidgetService} from "../widget.service";

@Component({
    selector: 'rs-widget-outlet',
    templateUrl: './widget-outlet.component.html'
})
export class WidgetOutletComponent implements OnInit {

    private showDiceWidget = false;

    constructor(private widgetService:WidgetService) {
    }

    ngOnInit() {
        this.widgetService.onToggleDiceWidget.subscribe(value => this.setDiceValue(value));
    }

    private setDiceValue(value) {
        return this.showDiceWidget = value;
    }
}