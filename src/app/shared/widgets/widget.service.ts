import {Injectable, EventEmitter, Output, OnDestroy} from "@angular/core";
import {Router} from "@angular/router";
import {WidgetOnScrollDirective} from "./directives/widget-on-scroll.directive";

@Injectable()
export class WidgetService {

    private showDiceWidget = false;
    widgetOnScroll: WidgetOnScrollDirective;

    constructor(private router:Router){
        router.events.subscribe(value => this.hideWidgets());
    }

    onToggleDiceWidget = new EventEmitter<boolean>();

    onScroll = new EventEmitter<number>();

    toggleDiceWidget() {
        this.showDiceWidget = !this.showDiceWidget;
        this.onToggleDiceWidget.emit(this.showDiceWidget);
    }

    scrolled(number: number) {
        this.onScroll.emit(number);
    }

    private hideWidgets() {
        this.showDiceWidget = false;
        this.onToggleDiceWidget.emit(this.showDiceWidget);
    }
}